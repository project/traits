<?php

namespace Drupal\traits\Traits;

/**
 * Trait DefaultOptionsTrait.
 *
 * @package Drupal\traits\Traits
 */
trait DefaultOptionsTrait {

  /**
   * Merge options with default values.
   *
   * @param array $input
   *   The input array.
   * @param array $defaults
   *   The default array.
   *
   * @return array
   *   The options.
   */
  private function merge(array $input = [], array $defaults = []) {
    return array_merge(
      $defaults,
      array_intersect_key($input, $defaults)
    );
  }

  /**
   * Default options are limited to the top 2 levels.
   *
   * @param array $input
   *   The input.
   * @param array $defaults
   *   The 2 level defaults array at least.
   *
   * @return array
   *   The options with default values.
   */
  private function twoLevelsMerge(array $input = [], array $defaults = []) {
    $first_level = $this->merge($input, $defaults);
    foreach ($input as $key => $second_level_input) {
      if (is_array($second_level_input) && array_key_exists($key, $first_level)) {
        $second_level_defaults = $defaults[$key];
        $first_level[$key] = $this->merge($second_level_input, $second_level_defaults);
      }
    }
    return $first_level;
  }

}
