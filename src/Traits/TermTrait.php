<?php


namespace Drupal\traits\Traits;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Trait TermTrait.
 *
 * @package Drupal\traits\Traits
 */
trait TermTrait {

  /**
   * Get tid by name.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param int $vid
   *   The vocabulary id.
   * @param string $name
   *   The name.
   * @param array $terms
   *   Serve as the cache.
   *
   * @return int|false
   *   The tid.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTid(EntityTypeManagerInterface $entity_type_manager, $vid, $name, array &$terms = []) {
    // Read from cache.
    if (isset($terms[$vid][$name])) {
      return $terms[$vid][$name];
    }

    $properties = [
      'name' => $name,
      'vid' => $vid,
    ];

    $storage = $entity_type_manager->getStorage('taxonomy_term');
    $terms = $storage->loadByProperties($properties);
    if (!empty($terms)) {
      $term = reset($terms);
      $tid = $term->id();
      // Save result to cache.
      $terms[$vid][$name] = $tid;
      return $tid;
    }
    return FALSE;
  }

  /**
   * Get tid by name if the term does not exist, create a new one.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param int $vid
   *   The vocabulary id.
   * @param string $name
   *   The name.
   * @param array $terms
   *   Serve as the cache.
   *
   * @return int
   *   The tid.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getTidCreateIfNotExist(EntityTypeManagerInterface $entity_type_manager, $vid, $name, array &$terms = []) {
    $tid = $this->getTid($entity_type_manager, $vid, $name, $terms);
    if ($tid === FALSE) {
      $storage = $entity_type_manager->getStorage('taxonomy_term');
      $term = $storage->create(
        [
          'name' => $name,
          'vid' => $vid,
        ]);
      $term->save();
      $tid = $term->id();
      $terms[$vid][$name] = $tid;
    }
    return $tid;
  }

  /**
   * Add terms to vocabulary.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param string $vid
   *   The vid.
   * @param array $term_names
   *   The term names.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addTerms(EntityTypeManagerInterface $entity_type_manager, $vid, array $term_names) {
    $storage = $entity_type_manager->getStorage('taxonomy_term');
    if (!empty($term_names)) {
      foreach ($term_names as $term_name) {
        if (!$this->isTermExisted($entity_type_manager, $vid, $term_name)) {
          $term_data = [
            'name' => $term_name,
            'vid' => $vid,
          ];
          $term = $storage->create($term_data);
          $term->save();
        }
      }
    }
  }

  /**
   * Check if term exists.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param string $vid
   *   The vid.
   * @param string $name
   *   The term name to check.
   *
   * @return bool
   *   Exist or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function isTermExisted(EntityTypeManagerInterface $entity_type_manager, $vid, $name) {
    $storage = $entity_type_manager->getStorage('taxonomy_term');
    $query = $storage->getQuery();
    $query->condition('vid', $vid);
    $query->condition('name', $name);
    $result = $query->execute();
    if (!empty($result)) {
      return TRUE;
    }
    return FALSE;

  }

  /**
   * Create vocabulary.
   *
   * If it's existed, update the label if gets a new label.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param string $vid
   *   The vid.
   * @param string $vocabulary_label
   *   The vocabulary label.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createVocabulary(EntityTypeManagerInterface $entity_type_manager, $vid, $vocabulary_label) {

    $vocabulary_data = [
      'name' => $vocabulary_label,
      'vid' => $vid,
    ];

    $storage = $entity_type_manager->getStorage('taxonomy_vocabulary');

    $vocabulary = $storage->load($vid);

    if ($vocabulary === NULL) {
      $vocabulary = $storage->create($vocabulary_data);
      $vocabulary->save();
    }
    elseif ($vocabulary->label() !== $vocabulary_label) {
      $vocabulary->name = $vocabulary_label;
      $vocabulary->save();
    }
  }

}
