<?php

namespace Drupal\traits\Traits;

use Drupal\Component\Utility\Random;
use Drupal\Core\Language\LanguageInterface;

/**
 * Trait MachineNameTrait.
 *
 * @package Drupal\traits\Traits
 */
trait MachineNameTrait {

  /**
   * The random generator.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $randomGenerator;

  /**
   * Get machine name.
   *
   * @param string $value
   *   The input.
   * @param mixed $default
   *   The default value.
   *
   * @return string
   *   The machine name.
   */
  protected function getMachineName($value, $default = FALSE) {
    $new_value = \Drupal::transliteration()->transliterate($value, LanguageInterface::LANGCODE_DEFAULT, '_');
    $new_value = strtolower($new_value);
    $new_value = preg_replace('/[^a-z0-9_]+/', '_', $new_value);
    $machine_name = preg_replace('/_+/', '_', $new_value);
    if (empty($machine_name)) {
      return $default;
    }
    return $machine_name;
  }

  /**
   * Generates a unique random string containing letters and numbers.
   *
   * Do not use this method when testing unvalidated user input. Instead, use
   * \Drupal\simpletest\TestBase::randomString().
   *
   * @param int $length
   *   Length of random string to generate.
   *
   * @return string
   *   Randomly generated unique string.
   *
   * @see \Drupal\Component\Utility\Random::name()
   */
  protected function randomMachineName($length = 8) {
    return strtolower($this->getRandomGenerator()->word($length));
  }

  /**
   * Gets the random generator for the utility methods.
   *
   * @return \Drupal\Component\Utility\Random
   *   The random generator.
   */
  private function getRandomGenerator() {
    if (!is_object($this->randomGenerator)) {
      $this->randomGenerator = new Random();
    }
    return $this->randomGenerator;
  }

}
