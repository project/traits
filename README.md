# Traits

PHP [Traits](https://www.php.net/manual/en/language.oop5.traits.php) are handy and useful.

Imagining you want to get the term ID by term name from a vocabulary. If there is a trait called TermTraits which has a method called `getTermIdByName($vid, $term_name`). Your code would be cleaner and simpler.

```php
traits TermTraits {
  public function getTermIdByName($vid, $term_name) {
    // ...
    retrun $tid;
  }
}

class YourClassName {
  use TermTraits;

  public function yourMethod() {
    // ...
    $tid = this-> getTermIdByName($vid, $term_name);
  }
}
```

Traits are only able to be used in classes and other traits. it's unable to be used in procedural code, such as hook implementations. However, it's still possible to copy and paste code from traits if necessary.

So this module aims to

- **Direct use**: provide neat traits to developers
- **Indirect use**: as ***a trait\*** injects to the well-known [Examples for Developers](https://www.drupal.org/project/examples) module -- being examples or code snippets to developers
